#!/usr/bin/env python3

import urllib3
import json as j
import csv
import argparse
import subprocess as sub
import tqdm
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
import elasticsearch.exceptions
import masker

def file_reader(file_in):
    with open(file_in, "r", errors="ignore") as f:
        for line in f:
            clean = line.strip("\n")
            yield clean

#function to read index settings from file
def create_index(client, file_in, index_name):
    with open(file_in, "r") as index_settings:
        data = index_settings.read()
    try:
        client.indices.create(
            index_name,
            body = j.loads(data)
        )
    except elasticsearch.exceptions.RequestError as index_error:
        print("Index already made!")

# bulk insert function for csv

def bulk_insert_csv(file_in):
    with open(file_in, "r") as f:
        reader = csv.DictReader(f)
        for row in reader:
            doc = {
                "username": row["username"],
                "password": row["password"],
                "mask": row["mask"],
                "domain": row["domain"],
                "password-length": row["password-length"]
            }
            yield doc

# funct to read lines in a csv file

def csv_count(file_in):
    with open(file_in, "r", encoding="UTF-8") as f:
        reader = csv.DictReader(f)
        while True:
            try:
                row_count = sum(1 for row in reader)
                return row_count
            except csv.Error:
                continue



def bulk_ndjson(file_in):
    for line in file_reader(file_in):
        doc = line
        yield doc

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--es_host", help="ElasticSearch host",
                        required=True)
    parser.add_argument("-i", "--index", help="Index to store data in",
                        required=True)
    parser.add_argument("-s", "--index_settings", help="file conating settings",
                        required=True, default="settings.json")
    parser.add_argument("-f", "--file_in", help="File to index",
                        required=True)
    parser.add_argument("-j", "--ndjson", help="read Ndjson format",
                        action="store_true")
    parser.add_argument("-r", "--raw", help="Read raw email:pass and index it", action="store_true")
    parser.add_argument("-c", "--csv", help="Read csv file and index it", action="store_true")
    args = parser.parse_args()
    Client = Elasticsearch([
        {"host": f"{args.es_host}", "timeout": 60}
    ])

    print("Creating index...")
    create_index(Client, args.index_settings, args.index)
    print(f"Counting lines in {args.file_in}, may take a while please wait....")
    total_docs = int(csv_count(args.file_in))
    print("Starting docs upload....")
    progress = tqdm.tqdm(unit="docs", total=total_docs)
    successes = 0
    try:
        if args.csv:
            for ok, action in streaming_bulk(
                    client=Client, index=args.index,
                    actions=bulk_insert_csv(args.file_in)):
                progress.update(1)
                successes += ok
        elif args.ndjson:
            for ok, action in streaming_bulk(
                    client=Client, index=args.index,
                    actions=bulk_ndjson(args.file_in)):
                progress.update(1)
                successes += ok
    except SystemExit as e:
        print(e)
    print("Done")
main()
